package groupRepository

import (
	"github.com/abdugania/api/models"
	"github.com/abdugania/api/repository"
)

const (
	AllGroups   = "group/all"
	GetGroup    = "group/get"
	UpdateGroup = "group/update"
	CreateGroup = "group/add"
	DeleteGroup = "group/delete"
)

type GroupRepository struct {
	repository.Repository
}

func (g GroupRepository) GetAll(args models.GroupSearch) (groups models.Groups, err error) {

	err = repository.DbConn.Raw(repository.Sqls.Get(AllGroups), args.Count, args.Start).Scan(&groups).Error
	return
}

func (g GroupRepository) Get(args models.Group) (group models.Group, err error) {
	err = repository.DbConn.Raw(repository.Sqls.Get(GetGroup), args.ID).Scan(&group).Error
	return
}

func (g GroupRepository) Update(group models.Group) error {
	return repository.DbConn.Exec(repository.Sqls.Get(UpdateGroup),
		group.Name,
		group.Description,
		group.IsActive,
		group.UpdatedAt,
		group.ID).Error
}

func (g GroupRepository) Remove(group models.Group) error {
	return repository.DbConn.Exec(repository.Sqls.Get(DeleteGroup),
		group.ID).Error
}

func (g GroupRepository) Add(group models.Group) (id int, err error) {
	err = repository.DbConn.Exec(repository.Sqls.Get(CreateGroup), group.Name, group.Description, group.IsActive).Scan(&id).Error
	return
}
