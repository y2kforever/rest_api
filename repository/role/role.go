package roleRepository

import (
	"github.com/abdugania/api/models"
	"github.com/abdugania/api/repository"
)

const (
	AllRoles   = "role/all"
	GetRole    = "role/get"
	UpdateRole = "role/update"
	CreateRole = "role/add"
	DeleteRole = "role/delete"
)

type RoleRepository struct {
	repository.Repository
}

func (r RoleRepository) GetAll(args models.RoleSearch) (roles models.Roles, err error) {
	err = repository.DbConn.Raw(repository.Sqls.Get(AllRoles), args.Count, args.Start).Scan(&roles).Error
	return
}

func (r RoleRepository) Get(args models.Role) (role models.Role, err error) {
	err = repository.DbConn.Raw(repository.Sqls.Get(GetRole), args.ID).Scan(&role).Error
	return
}

func (r RoleRepository) Update(role models.Role) error {
	return repository.DbConn.Exec(repository.Sqls.Get(UpdateRole),
		role.Name,
		role.Alias,
		role.Description,
		role.IsActive,
		role.UpdatedAt,
		role.ID).Error
}

func (r RoleRepository) Remove(role models.Role) error {
	return repository.DbConn.Exec(repository.Sqls.Get(DeleteRole),
		role.ID).Error
}

func (r RoleRepository) Add(role models.Role) (id int, err error) {
	err = repository.DbConn.Exec(repository.Sqls.Get(CreateRole),
		role.Name,
		role.Alias,
		role.Description,
		role.IsActive).Scan(&id).Error
	return
}
