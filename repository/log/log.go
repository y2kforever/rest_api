package logRepository

import (
	"github.com/abdugania/api/models"
	"github.com/abdugania/api/repository"
)

const (
	AllLogs   = "log/all"
	GetLog    = "log/get"
	UpdateLog = "log/update"
	CreateLog = "log/add"
	DeleteLog = "log/delete"
)

type LogRepository struct {
	repository.Repository
}

func (l LogRepository) GetAll(args models.LogSearch) (logs models.Logs, err error) {
	err = repository.DbConn.Raw(repository.Sqls.Get(AllLogs), args.Count, args.Start).Scan(&logs).Error
	return
}

func (l LogRepository) Get(args models.Log) (log models.Log, err error) {
	err = repository.DbConn.Raw(repository.Sqls.Get(GetLog), args.ID).Scan(&log).Error
	return
}

func (l LogRepository) Update(log models.Log) error {
	return repository.DbConn.Exec(repository.Sqls.Get(UpdateLog),
		log.Name,
		log.Body,
		log.ID).Error
}

func (l LogRepository) Remove(log models.Log) error {
	return repository.DbConn.Exec(repository.Sqls.Get(DeleteLog),
		log.ID).Error
}

func (l LogRepository) Add(log models.Log) (id int, err error) {
	err = repository.DbConn.Exec(repository.Sqls.Get(CreateLog), log.Name, log.Body).Scan(&id).Error
	return
}
