package deviceRepository

import (
	"github.com/abdugania/api/models"
	"github.com/abdugania/api/repository"
)

const (
	AllDevices   = "device/all"
	GetDevice    = "device/get"
	UpdateDevice = "device/update"
	CreateDevice = "device/add"
	DeleteDevice = "device/delete"
)

type DeviceRepository struct {
	repository.Repository
}

func (d DeviceRepository) GetAll(args models.DeviceSearch) (devices models.Devices, err error) {

	err = repository.DbConn.Raw(repository.Sqls.Get(AllDevices), args.Count, args.Start).Scan(&devices).Error
	return
}

func (d DeviceRepository) Get(args models.Device) (device models.Device, err error) {
	err = repository.DbConn.Raw(repository.Sqls.Get(GetDevice), args.ID).Scan(&device).Error
	return
}

func (d DeviceRepository) Update(device models.Device) error {
	return repository.DbConn.Exec(repository.Sqls.Get(UpdateDevice),
		device.Settings,
		device.IsActive,
		device.UpdatedAt,
		device.ID).Error
}

func (d DeviceRepository) Remove(device models.Device) error {
	return repository.DbConn.Exec(repository.Sqls.Get(DeleteDevice),
		device.ID).Error
}

func (d DeviceRepository) Add(device models.Device) (id int, err error) {
	err = repository.DbConn.Exec(repository.Sqls.Get(CreateDevice), device.Settings, device.IsActive).Scan(&id).Error
	return
}
