package repository

import (
	"github.com/abdugania/api/conf"
	"github.com/abdugania/api/database"
	"github.com/abdugania/api/models"
	"github.com/jinzhu/gorm"
	"github.com/ndrewnee/sqlreader"
)

var (
	DbConn *gorm.DB
	Cnf    *conf.Config
	Sqls   *sqlreader.SqlReader
)

type Repository interface {
	Find(id int) (models.User, error)
	FindAll() ([]models.User, error)
	Update(user models.User) error
	Remove(user models.User) (error)
}

func init() {
	var err error
	Cnf = conf.GetConf()
	DbConn = database.GetDb()

	Sqls, err = sqlreader.New(Cnf.SqlDir)

	if err != nil {
		panic(err)
	}
}
