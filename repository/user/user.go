package userRepository

import (
	"github.com/abdugania/api/models"
	"github.com/abdugania/api/repository"
)

const (
	AllUsers   = "user/all_users"
	GetUser    = "user/get_user"
	UpdateUser = "user/update"
	CreateUser = "user/add"
	DeleteUser = "user/delete"
)

type UserRepository struct {
	repository.Repository
}

func (u UserRepository) GetAll(args models.Search) (user models.Users, err error) {

	err = repository.DbConn.Raw(repository.Sqls.Get(AllUsers), args.Count, args.Start).Scan(&user).Error
	return
}

func (u UserRepository) Get(args models.User) (user models.User, err error) {
	err = repository.DbConn.Raw(repository.Sqls.Get(GetUser), args.ID).Scan(&user).Error
	return
}

func (u UserRepository) Update(user models.User) error {
	return repository.DbConn.Exec(repository.Sqls.Get(UpdateUser),
		user.StatusID,
		user.ID).Error
}

func (u UserRepository) Remove(user models.User) error {
	return repository.DbConn.Exec(repository.Sqls.Get(DeleteUser),
		user.ID).Error
}

func (u UserRepository) Add(user models.User) (id int, err error) {
	err = repository.DbConn.Exec(repository.Sqls.Get(CreateUser), user.StatusID).Scan(&id).Error
	return
}
