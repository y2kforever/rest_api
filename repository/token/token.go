package tokenRepository

import (
	"github.com/abdugania/api/models"
	"github.com/abdugania/api/repository"
	"github.com/google/uuid"
	"time"
)

const (
	AllTokens   = "token/all"
	GetToken    = "token/get_by_id"
	GetByToken  = "token/get_by_token"
	UpdateToken = "token/update"
	CreateToken = "token/add"
	DeleteToken = "token/delete"
)

type TokenRepository struct {
	repository.Repository
}

func (t TokenRepository) GetAll(args models.TokenSearch) (tokens models.Tokens, err error) {

	err = repository.DbConn.Raw(repository.Sqls.Get(AllTokens), args.Count, args.Start).Scan(&tokens).Error
	return
}

func (t TokenRepository) Get(args models.Token) (token models.Token, err error) {
	err = repository.DbConn.Raw(repository.Sqls.Get(GetToken), args.ID).Scan(&token).Error
	return
}

func (t TokenRepository) GetByToken(args models.Token) (token models.Token, err error) {
	err = repository.DbConn.Raw(repository.Sqls.Get(GetByToken), args.Token).Scan(&token).Error
	return
}

func (t TokenRepository) Update(token models.Token) error {
	return repository.DbConn.Exec(repository.Sqls.Get(UpdateToken),
		token.Token,
		token.Refresh,
		token.IsActive,
		token.ExpiredAt,
		token.ID).Error
}

func (t TokenRepository) Remove(args models.Token) error {
	return repository.DbConn.Exec(repository.Sqls.Get(DeleteToken),
		args.Token).Error
}

func (t TokenRepository) Add(args models.Token) (id int, err error) {

	token, _ := uuid.NewRandom()
	refresh, _ := uuid.NewRandom()
	expiredAt := time.Now().AddDate(0, 1, 0)

	err = repository.DbConn.Exec(repository.Sqls.Get(CreateToken), token, refresh, args.IsActive, expiredAt).Scan(&id).Error
	return
}
