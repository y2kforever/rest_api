package repository

import (
	"github.com/abdugania/api/conf"
)

func init() {
	conf.SetRunMode(conf.ModeTest)
}
