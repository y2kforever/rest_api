package permissionRepository

import (
	"github.com/abdugania/api/models"
	"github.com/abdugania/api/repository"
)

const (
	AllPermissions   = "permission/all"
	GetPermission    = "permission/get"
	UpdatePermission = "permission/update"
	CreatePermission = "permission/add"
	DeletePermission = "permission/delete"
)

type PermissionRepository struct {
	repository.Repository
}

func (p PermissionRepository) GetAll(args models.PermissionSearch) (permissions models.Permissions, err error) {
	err = repository.DbConn.Raw(repository.Sqls.Get(AllPermissions), args.Count, args.Start).Scan(&permissions).Error
	return
}

func (p PermissionRepository) Get(args models.Permission) (permission models.Permission, err error) {
	err = repository.DbConn.Raw(repository.Sqls.Get(GetPermission), args.ID).Scan(&permission).Error
	return
}

func (p PermissionRepository) Update(permission models.Permission) error {
	return repository.DbConn.Exec(repository.Sqls.Get(UpdatePermission),
		permission.Name,
		permission.Alias,
		permission.Description,
		permission.UpdatedAt,
		permission.ID).Error
}

func (p PermissionRepository) Remove(permission models.Permission) error {
	return repository.DbConn.Exec(repository.Sqls.Get(DeletePermission),
		permission.ID).Error
}

func (p PermissionRepository) Add(permission models.Permission) (id int, err error) {
	err = repository.DbConn.Exec(repository.Sqls.Get(CreatePermission),
		permission.Name,
		permission.Alias,
		permission.Description).Scan(&id).Error
	return
}
