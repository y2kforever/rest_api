package main

import (
	"github.com/abdugania/api/controllers"
	"net/http"
	"time"
)

const (
	defaultPort       = "8000"
	idleTimeout       = 30 * time.Second
	writeTimeout      = 180 * time.Second
	readHeaderTimeout = 10 * time.Second
	readTimeout       = 10 * time.Second
)

func main() {

	handler := controllers.Init()

	server := &http.Server{
		Addr:    "0.0.0.0:" + defaultPort,
		Handler: handler,

		IdleTimeout:       idleTimeout,
		WriteTimeout:      writeTimeout,
		ReadHeaderTimeout: readHeaderTimeout,
		ReadTimeout:       readTimeout,
	}

	server.ListenAndServe()
}
