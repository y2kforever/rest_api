package conf

import (
	"testing"

	"gopkg.in/stretchr/testify.v1/require"

	"gopkg.in/stretchr/testify.v1/assert"
)

func TestNewConfig(t *testing.T) {
	expect := assert.New(t)

	cfg, err := New("conf_sample.yaml")
	expect.NoError(err)
	expect.Equal("/var/log/tracking-service/", cfg.Log.Path)
	expect.Equal("host=localhost port=5432 user=postgres password=postgres dbname=test sslmode=disable", cfg.Databases.GetDSN())

	t.Run("Should check function SetRunMode", func(t *testing.T) {
		SetRunMode(ModeTest)
		expect.Equal("../conf", confPath)
		expect.Equal("../database/sql", sqlDir)

		require.NotPanics(t, func() {
			SetRunMode(ModeProd)
		})
	})
}
