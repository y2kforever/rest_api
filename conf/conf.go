package conf

import (
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"path/filepath"
	"sync"
)

const (
	ModeProd = "prod"
	ModeTest = "test"
)

var (
	cfg  *Config
	once sync.Once

	confPath = "conf"
	sqlDir   = "database/postgres/sql"

	fileNameMap = map[string]string{
		ModeProd: "conf.yaml",
		ModeTest: "conf_test.yaml",
	}

	fileName = fileNameMap[ModeProd]
)

type Database struct {
	Dialect  string `yaml:"dialect"`
	Host     string `yaml:"host"`
	DbName   string `yaml:"dbname"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	Port     string `yaml:"port"`
	SSL      string `yaml:"ssl"`
}

// GetDSN  need to make dsn connection string
func (db *Database) GetDSN() string {
	switch db.Dialect {
	case "mysql":
		return fmt.Sprintf("%s:%s@tcp(%s:%v)/%s?parseTime=true", db.User, db.Password, db.Host, db.Port, db.DbName)
	case "postgres":
		return fmt.Sprintf("host=%s port=%v user=%s password=%s dbname=%s sslmode=%s", db.Host, db.Port, db.User, db.Password, db.DbName, db.SSL)
	default:
		return ""
	}
}

type Config struct {
	Log struct {
		ToFile bool   `yaml:"to_file"`
		Path   string `yaml:"path"`
		Json   bool   `yaml:"json"`
		Level  string `yaml:"level"`
	} `yaml:"log"`

	Databases Database `yaml:"database"`
	SqlDir    string   `yaml:"sql_dir"`
}

func (c *Config) SetDefaults() {
	c.Log.ToFile = true
	c.Log.Json = false
	c.Log.Path = "/var/log/auth-api/"
	c.Log.Level = "info"
	c.Databases.Dialect = "postgres"
	c.Databases.DbName = "core"
	c.Databases.Host = "localhost"
	c.Databases.Port = "54321"
	c.Databases.User = "api"
	c.Databases.Password = "secret"
	c.Databases.SSL = "disable"
	c.SqlDir = sqlDir
}

func GetConf() *Config {
	once.Do(func() {
		var err error
		filePath := confPath + string(os.PathSeparator) + fileName
		cfg, err = New(filePath)
		if err != nil {
			panic(err)
		}
	})

	return cfg
}

func New(filePath ...string) (cf *Config, err error) {
	cf = new(Config)
	cf.SetDefaults()

	if len(filePath) > 0 {
		var fileBytes []byte
		fileBytes, err = ioutil.ReadFile(filePath[0])
		if err != nil {
			return
		}

		err = yaml.Unmarshal(fileBytes, cf)
		if err != nil {
			return
		}
	}

	return
}

func SetRunMode(mode string) {
	file, ok := fileNameMap[mode]
	if !ok {
		return
	}

	fileName = file

	switch mode {
	case ModeTest:
		confPath = ".." + string(os.PathSeparator) + confPath
		sqlDir = ".." + string(os.PathSeparator) + sqlDir
	case ModeProd:
		dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
		if err != nil {
			panic(err)
		}

		confPath = dir + string(os.PathSeparator) + confPath
	}
}
