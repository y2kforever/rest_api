package models

type (
	Device struct {
		ID        int    `gorm:"column:id"`
		Settings  string `gorm:"column:settings"`
		IsActive  bool   `gorm:"column:is_active"`
		UpdatedAt string `gorm:"column:updated_at"`
	}

	DeviceJson struct {
		ID        int    `json:"id"`
		Settings  string `json:"settings"`
		IsActive  bool   `json:"is_active"`
		UpdatedAt string `json:"updated_at"`
	}

	Devices []Token

	DeviceSearch struct {
		Count int
		Start int
	}
)
