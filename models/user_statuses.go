package models

type (
	UserStatus struct {
		ID          int `gorm:"column:id"`
		Name        int `gorm:"column:name"`
		Alias       int `gorm:"column:alias"`
		Description int `gorm:"column:description"`
	}

	UserStatusJson struct {
		ID          int `json:"id"`
		Name        int `json:"name"`
		Alias       int `json:"alias"`
		Description int `json:"description"`
	}

	UserStatuses []UserStatus

	UserStatusesSearch struct {
		Count int
		Start int
	}
)
