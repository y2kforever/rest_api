package models

type (
	RoleGroup struct {
		DeviceID int `gorm:"column:role_id"`
		TokenID  int `gorm:"column:group_id"`
	}

	RoleGroupJson struct {
		DeviceID int `json:"role_id"`
		TokenID  int `json:"group_id"`
	}

	RolesGroups []RoleGroup

	RolesGroupsSearch struct {
		Count int
		Start int
	}
)
