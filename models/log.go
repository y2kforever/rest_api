package models

type (
	Log struct {
		ID   int    `gorm:"column:id"`
		Name string `gorm:"column:name"`
		Body string `gorm:"column:body"`
	}

	LogJson struct {
		ID   int    `json:"id"`
		Name string `json:"name"`
		Body string `json:"body"`
	}

	Logs []Log

	LogSearch struct {
		Count int
		Start int
	}
)
