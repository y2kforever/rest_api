package models

type (
	WebAgent struct {
		ID   int    `gorm:"column:id"`
		Body string `gorm:"column:body"`
	}

	WebAgentJson struct {
		ID   int    `json:"id"`
		Body string `json:"body"`
	}

	WebAgents []WebAgent

	WebAgentSearch struct {
		Count int
		Start int
	}
)
