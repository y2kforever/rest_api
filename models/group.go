package models

type (
	Group struct {
		ID          int    `gorm:"column:id"`
		Name        string `gorm:"column:name"`
		Description string `gorm:"column:description"`
		IsActive    bool   `gorm:"column:is_active"`
		UpdatedAt   string `gorm:"column:updated_at"`
	}

	GroupJson struct {
		ID          int    `json:"id"`
		Name        string `json:"name"`
		Description string `json:"description"`
		IsActive    bool   `json:"is_active"`
		UpdatedAt   string `json:"updated_at"`
	}

	Groups []Group

	GroupSearch struct {
		Count int
		Start int
	}
)
