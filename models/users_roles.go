package models

type (
	UserRole struct {
		UserID int `gorm:"column:user_id"`
		RoleID int `gorm:"column:role_id"`
	}

	UserRoleJson struct {
		UserID int `json:"user_id"`
		RoleID int `json:"role_id"`
	}

	UsersRoles []UserRole

	UsersRolesSearch struct {
		Count int
		Start int
	}
)
