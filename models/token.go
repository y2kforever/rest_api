package models

type (
	Token struct {
		ID        int    `gorm:"column:id"`
		Token     string `gorm:"column:token"`
		Refresh   string `gorm:"column:refresh"`
		IsActive  bool   `gorm:"column:is_active"`
		ExpiredAt string `gorm:"column:expired_at"`
	}

	TokenJson struct {
		ID        int    `json:"id"`
		Token     string `json:"token"`
		Refresh   string `json:"refresh"`
		IsActive  bool   `json:"is_active"`
		ExpiredAt string `json:"expired_at"`
	}

	Tokens []Token

	TokenSearch struct {
		Count int
		Start int
	}
)
