package models

type (
	Session struct {
		DeviceID int `gorm:"column:device_id"`
		AgentID  int `gorm:"column:agent_id"`
	}

	SessionJson struct {
		DeviceID int `json:"device_id"`
		AgentID  int `json:"agent_id"`
	}

	Sessions []DeviceToken

	SessionsSearch struct {
		Count int
		Start int
	}
)
