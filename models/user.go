package models

type (
	User struct {
		ID       int    `gorm:"column:id"`
		StatusID int    `gorm:"column:status_id"`
		Settings string `gorm:"column:settings"`
	}

	UserJson struct {
		ID       int    `json:"id"`
		StatusID int    `json:"status_id"`
		Settings string `json:"settings"`
	}

	Users []User

	Search struct {
		Count int
		Start int
	}
	Settings struct{}
)
