package models

type (
	RolePermission struct {
		DeviceID int `gorm:"column:role_id"`
		TokenID  int `gorm:"column:permission_id"`
	}

	RolePermissionJson struct {
		DeviceID int `json:"role_id"`
		TokenID  int `json:"permission_id"`
	}

	RolesPermissions []RolePermission

	RolesPermissionsSearch struct {
		Count int
		Start int
	}
)
