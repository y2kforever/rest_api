package models

type (
	UserDevice struct {
		UserID   int `gorm:"column:user_id"`
		DeviceID int `gorm:"column:device_id"`
	}

	UserDeviceJson struct {
		UserID   int `json:"user_id"`
		DeviceID int `json:"device_id"`
	}

	UsersDevices []UserDevice

	UsersDevicesSearch struct {
		Count int
		Start int
	}
)
