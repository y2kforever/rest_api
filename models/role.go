package models

type (
	Role struct {
		ID          int    `gorm:"column:id"`
		Name        string `gorm:"column:name"`
		Alias       string `gorm:"column:alias"`
		Description string `gorm:"column:description"`
		IsActive    bool   `gorm:"column:is_active"`
		UpdatedAt   string `gorm:"column:updated_at"`
	}

	RoleJson struct {
		ID          int    `json:"id"`
		Name        string `json:"name"`
		Alias       string `json:"alias"`
		Description string `json:"description"`
		IsActive    bool   `json:"is_active"`
		UpdatedAt   string `json:"updatedAt"`
	}

	Roles []Role

	RoleSearch struct {
		Count int
		Start int
	}
)
