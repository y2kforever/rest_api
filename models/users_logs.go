package models

type (
	UserLog struct {
		UserID int `gorm:"column:user_id"`
		LogID  int `gorm:"column:log_id"`
	}

	UserLogJson struct {
		UserID int `json:"user_id"`
		LogID  int `json:"log_id"`
	}

	UsersLogs []UserLog

	UsersLogsSearch struct {
		Count int
		Start int
	}
)
