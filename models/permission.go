package models

type (
	Permission struct {
		ID          int    `gorm:"column:id"`
		Name        string `gorm:"column:name"`
		Alias       string `gorm:"column:alias"`
		Description string `gorm:"column:description"`
		UpdatedAt   string `gorm:"column:updated_at"`
	}

	PermissionJson struct {
		ID          int    `json:"id"`
		Name        string `json:"name"`
		Alias       string `json:"alias"`
		Description string `json:"description"`
		UpdatedAt   string `json:"updatedAt"`
	}

	Permissions []Permission

	PermissionSearch struct {
		Count int
		Start int
	}
)
