package models

type (
	DeviceToken struct {
		DeviceID int `gorm:"column:device_id"`
		TokenID  int `gorm:"column:token_id"`
	}

	DeviceTokenJson struct {
		DeviceID int `json:"device_id"`
		TokenID  int `json:"token_id"`
	}

	DevicesTokens []DeviceToken

	DevicesTokensSearch struct {
		Count int
		Start int
	}
)
