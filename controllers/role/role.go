package role

import (
	"encoding/json"
	"github.com/abdugania/api/models"
	"github.com/abdugania/api/repository/role"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"
	"strconv"
)

var roleRepo roleRepository.RoleRepository

func All(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	count, _ := strconv.Atoi(params["count"])
	start, _ := strconv.Atoi(params["start"])

	if count > 10 || count < 1 {
		count = 10
	}
	if start < 0 {
		start = 0
	}

	arg := models.RoleSearch{Start: start, Count: count}
	roles, _ := roleRepo.GetAll(arg)
	js, err := json.Marshal(roles)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func Add(w http.ResponseWriter, r *http.Request) {

	var roleJson models.RoleJson

	body, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(body, &roleJson)

	role := models.Role{
		Name:        roleJson.Name,
		Alias:       roleJson.Alias,
		Description: roleJson.Description,
		IsActive:    roleJson.IsActive,
	}

	id, _ := roleRepo.Add(role)

	json.NewEncoder(w).Encode(id)
}

func Get(w http.ResponseWriter, r *http.Request) {
	var role models.Role
	params := mux.Vars(r)

	id, _ := strconv.Atoi(params["id"])
	arg := models.Role{ID: id}
	role, _ = roleRepo.Get(arg)

	json.NewEncoder(w).Encode(role)
}

func Update(w http.ResponseWriter, r *http.Request) {
	var roleJson models.RoleJson
	params := mux.Vars(r)

	id, _ := strconv.Atoi(params["id"])
	body, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(body, &roleJson)

	role := models.Role{
		ID:          id,
		Name:        roleJson.Name,
		Alias:       roleJson.Alias,
		Description: roleJson.Description,
		IsActive:    roleJson.IsActive,
		UpdatedAt:   roleJson.UpdatedAt,
	}

	rowsUpdated := roleRepo.Update(role)
	json.NewEncoder(w).Encode(rowsUpdated)
}

func Delete(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])

	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid User ID")
		return
	}

	role := models.Role{ID: id}
	rowsDeleted := roleRepo.Remove(role)
	json.NewEncoder(w).Encode(rowsDeleted)
}

func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
