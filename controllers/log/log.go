package log

import (
	"encoding/json"
	"github.com/abdugania/api/models"
	"github.com/abdugania/api/repository/log"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"
	"strconv"
)

var logRepo logRepository.LogRepository

func All(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	count, _ := strconv.Atoi(params["count"])
	start, _ := strconv.Atoi(params["start"])

	if count > 10 || count < 1 {
		count = 10
	}
	if start < 0 {
		start = 0
	}

	arg := models.LogSearch{Start: start, Count: count}
	groups, _ := logRepo.GetAll(arg)
	js, err := json.Marshal(groups)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func Add(w http.ResponseWriter, r *http.Request) {

	var logJson models.LogJson

	body, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(body, &logJson)

	group := models.Log{
		Name: logJson.Name,
		Body: logJson.Body,
	}

	id, _ := logRepo.Add(group)

	json.NewEncoder(w).Encode(id)
}

func Get(w http.ResponseWriter, r *http.Request) {
	var log models.Log
	params := mux.Vars(r)

	id, _ := strconv.Atoi(params["id"])
	arg := models.Log{ID: id}
	log, _ = logRepo.Get(arg)

	json.NewEncoder(w).Encode(log)
}

func Update(w http.ResponseWriter, r *http.Request) {}

func Delete(w http.ResponseWriter, r *http.Request) {}
