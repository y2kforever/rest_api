package group

import (
	"encoding/json"
	"github.com/abdugania/api/models"
	"github.com/abdugania/api/repository/group"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"
	"strconv"
)

var groupRepo groupRepository.GroupRepository

func All(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	count, _ := strconv.Atoi(params["count"])
	start, _ := strconv.Atoi(params["start"])

	if count > 10 || count < 1 {
		count = 10
	}
	if start < 0 {
		start = 0
	}

	arg := models.GroupSearch{Start: start, Count: count}
	groups, _ := groupRepo.GetAll(arg)
	js, err := json.Marshal(groups)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func Add(w http.ResponseWriter, r *http.Request) {

	var groupJson models.GroupJson

	body, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(body, &groupJson)

	group := models.Group{
		Name:        groupJson.Name,
		Description: groupJson.Description,
		IsActive:    groupJson.IsActive,
	}

	id, _ := groupRepo.Add(group)

	json.NewEncoder(w).Encode(id)
}

func Get(w http.ResponseWriter, r *http.Request) {
	var group models.Group
	params := mux.Vars(r)

	id, _ := strconv.Atoi(params["id"])
	arg := models.Group{ID: id}
	group, _ = groupRepo.Get(arg)

	json.NewEncoder(w).Encode(group)
}

func Update(w http.ResponseWriter, r *http.Request) {
	var groupJson models.Group
	params := mux.Vars(r)

	id, _ := strconv.Atoi(params["id"])
	body, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(body, &groupJson)

	group := models.Group{
		ID:          id,
		Name:        groupJson.Name,
		Description: groupJson.Description,
		IsActive:    groupJson.IsActive,
		UpdatedAt:   groupJson.UpdatedAt,
	}

	rowsUpdated := groupRepo.Update(group)
	json.NewEncoder(w).Encode(rowsUpdated)
}

func Delete(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])

	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid User ID")
		return
	}

	group := models.Group{ID: id}
	rowsDeleted := groupRepo.Remove(group)
	json.NewEncoder(w).Encode(rowsDeleted)
}

func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
