package controllers

import (
	"github.com/abdugania/api/controllers/device"
	"github.com/abdugania/api/controllers/group"
	"github.com/abdugania/api/controllers/permission"
	"github.com/abdugania/api/controllers/role"
	"github.com/abdugania/api/controllers/token"
	"github.com/abdugania/api/controllers/user"
	"github.com/gorilla/mux"
	"net/http"
)

func Init() http.Handler {

	router := mux.NewRouter()

	//UserController
	router.HandleFunc("/users", user.All).Methods(http.MethodGet)
	router.HandleFunc("/user", user.Add).Methods(http.MethodPost)
	router.HandleFunc("/user/{id:[0-9]+}", user.Get).Methods(http.MethodGet)
	router.HandleFunc("/user/{id:[0-9]+}", user.Update).Methods(http.MethodPut)
	router.HandleFunc("/user/{id:[0-9]+}", user.Delete).Methods(http.MethodDelete)

	//DeviceController
	router.HandleFunc("/devices", device.All).Methods(http.MethodGet)
	router.HandleFunc("/device/{id:[0-9]+}", device.Get).Methods(http.MethodGet)
	router.HandleFunc("/device/{id:[0-9]+}", device.Add).Methods(http.MethodPost)
	router.HandleFunc("/device/{id:[0-9]+}", device.Update).Methods(http.MethodPut)
	router.HandleFunc("/device/{id:[0-9]+}", device.Delete).Methods(http.MethodDelete)

	//TokenController
	router.HandleFunc("/tokens", token.All).Methods(http.MethodGet)
	router.HandleFunc("/token", token.Get).Methods(http.MethodGet)
	router.HandleFunc("/token", token.Add).Methods(http.MethodPost)
	router.HandleFunc("/token", token.Update).Methods(http.MethodPut)
	router.HandleFunc("/token", token.Delete).Methods(http.MethodDelete)

	//GroupController
	router.HandleFunc("/groups", group.All).Methods(http.MethodGet)
	router.HandleFunc("/group/{id:[0-9]+}", group.Get).Methods(http.MethodGet)
	router.HandleFunc("/group/{id:[0-9]+}", group.Add).Methods(http.MethodPost)
	router.HandleFunc("/group/{id:[0-9]+}", group.Update).Methods(http.MethodPut)
	router.HandleFunc("/group/{id:[0-9]+}", group.Delete).Methods(http.MethodDelete)

	//RoleController
	router.HandleFunc("/roles", role.All).Methods(http.MethodGet)
	router.HandleFunc("/role/{id:[0-9]+}", role.Get).Methods(http.MethodGet)
	router.HandleFunc("/role/{id:[0-9]+}", role.Add).Methods(http.MethodPost)
	router.HandleFunc("/role/{id:[0-9]+}", role.Update).Methods(http.MethodPut)
	router.HandleFunc("/role/{id:[0-9]+}", role.Delete).Methods(http.MethodDelete)

	//PermissionController
	router.HandleFunc("/permissions", permission.All).Methods(http.MethodGet)
	router.HandleFunc("/permission/{id:[0-9]+}", permission.Get).Methods(http.MethodGet)
	router.HandleFunc("/permission/{id:[0-9]+}", permission.Add).Methods(http.MethodPost)
	router.HandleFunc("/permission/{id:[0-9]+}", permission.Update).Methods(http.MethodPut)
	router.HandleFunc("/permission/{id:[0-9]+}", permission.Delete).Methods(http.MethodDelete)

	//LogController
	router.HandleFunc("/logs", permission.All).Methods(http.MethodGet)
	router.HandleFunc("/log/{id:[0-9]+}", permission.Get).Methods(http.MethodGet)
	router.HandleFunc("/log/{id:[0-9]+}", permission.Add).Methods(http.MethodPost)

	return router

}

type ControllerInterface interface {
	Prepare()      //some processing before execution begins
	Get()          //method = GET processing
	Post()         //method = POST processing
	Delete()       //method = DELETE processing
	Put()          //method = PUT handling
	Head()         //method = HEAD processing
	Patch()        //method = PATCH treatment
	Options()      //method = OPTIONS processing
	Finish()       //executed after completion of treatment
	Render() error //method executed after the corresponding method to render the page
}
