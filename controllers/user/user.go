package user

import (
	"encoding/json"
	"github.com/abdugania/api/models"
	"github.com/abdugania/api/repository/user"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"
	"strconv"
)

var users models.Users
var userRepo userRepository.UserRepository

func All(w http.ResponseWriter, r *http.Request) {

	params := mux.Vars(r)

	userRepo := userRepository.UserRepository{}

	count, _ := strconv.Atoi(params["count"])
	start, _ := strconv.Atoi(params["start"])

	if count > 10 || count < 1 {
		count = 10
	}
	if start < 0 {
		start = 0
	}

	arg := models.Search{Start: start, Count: count}
	users, _ = userRepo.GetAll(arg)
	js, err := json.Marshal(users)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func Add(w http.ResponseWriter, r *http.Request) {

	var userJson models.UserJson
	var userID int

	body, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(body, &userJson)

	user := models.User{
		StatusID: userJson.StatusID,
		Settings: userJson.Settings,
	}

	userID, _ = userRepo.Add(user)

	json.NewEncoder(w).Encode(userID)

}

func Get(w http.ResponseWriter, r *http.Request) {

	var user models.User
	params := mux.Vars(r)
	users = models.Users{}

	id, _ := strconv.Atoi(params["id"])
	arg := models.User{ID: id}
	user, _ = userRepo.Get(arg)

	json.NewEncoder(w).Encode(user)
}

func Update(w http.ResponseWriter, r *http.Request) {
	var userJson models.UserJson
	params := mux.Vars(r)

	id, _ := strconv.Atoi(params["id"])
	body, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(body, &userJson)

	user := models.User{
		ID:       id,
		StatusID: userJson.StatusID,
		Settings: userJson.Settings,
	}

	rowsUpdated := userRepo.Update(user)

	json.NewEncoder(w).Encode(rowsUpdated)
}

func Delete(w http.ResponseWriter, r *http.Request) {

	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])

	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid User ID")
		return
	}

	user := models.User{ID: id}
	rowsDeleted := userRepo.Remove(user)

	json.NewEncoder(w).Encode(rowsDeleted)
}

func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
