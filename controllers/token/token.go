package token

import (
	"encoding/json"
	"fmt"
	"github.com/abdugania/api/models"
	"github.com/abdugania/api/repository/token"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"
	"strconv"
)

var tokens models.Tokens
var tokenRepo tokenRepository.TokenRepository

func All(w http.ResponseWriter, r *http.Request) {

	params := mux.Vars(r)

	count, _ := strconv.Atoi(params["count"])
	start, _ := strconv.Atoi(params["start"])

	if count > 10 || count < 1 {
		count = 10
	}
	if start < 0 {
		start = 0
	}

	arg := models.TokenSearch{Start: start, Count: count}

	tokens, _ = tokenRepo.GetAll(arg)
	json.NewEncoder(w).Encode(tokens)
	fmt.Fprintf(w, "%s", 1)
}

func Add(w http.ResponseWriter, r *http.Request) {

	var tokenJson models.TokenJson
	var tokenID int

	body, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(body, &tokenJson)

	token := models.Token{
		Token:     tokenJson.Token,
		Refresh:   tokenJson.Refresh,
		IsActive:  tokenJson.IsActive,
		ExpiredAt: tokenJson.ExpiredAt,
	}

	tokenID, _ = tokenRepo.Add(token)

	json.NewEncoder(w).Encode(tokenID)
}

func Get(w http.ResponseWriter, r *http.Request) {

	var token models.Token
	params := mux.Vars(r)

	id, _ := strconv.Atoi(params["id"])
	arg := models.Token{ID: id}
	token, _ = tokenRepo.Get(arg)

	json.NewEncoder(w).Encode(token)
}

func Update(w http.ResponseWriter, r *http.Request) {
	var tokenJson models.TokenJson
	params := mux.Vars(r)

	id, _ := strconv.Atoi(params["id"])
	body, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(body, &tokenJson)

	token := models.Token{
		ID:        id,
		Token:     tokenJson.Token,
		Refresh:   tokenJson.Refresh,
		IsActive:  tokenJson.IsActive,
		ExpiredAt: tokenJson.ExpiredAt,
	}

	rowsUpdated := tokenRepo.Update(token)

	json.NewEncoder(w).Encode(rowsUpdated)
}

func Delete(w http.ResponseWriter, r *http.Request) {

	token := models.Token{Token: ""}
	rowsDeleted := tokenRepo.Remove(token)

	json.NewEncoder(w).Encode(rowsDeleted)
}
