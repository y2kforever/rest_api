package device

import (
	"encoding/json"
	"github.com/abdugania/api/models"
	"github.com/abdugania/api/repository/device"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"
	"strconv"
)

var deviceRepo deviceRepository.DeviceRepository

func All(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	count, _ := strconv.Atoi(params["count"])
	start, _ := strconv.Atoi(params["start"])

	if count > 10 || count < 1 {
		count = 10
	}
	if start < 0 {
		start = 0
	}

	arg := models.DeviceSearch{Start: start, Count: count}
	devices, _ := deviceRepo.GetAll(arg)
	js, err := json.Marshal(devices)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func Add(w http.ResponseWriter, r *http.Request) {
	var deviceJson models.DeviceJson

	body, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(body, &deviceJson)

	device := models.Device{
		Settings: deviceJson.Settings,
		IsActive: deviceJson.IsActive,
	}

	id, _ := deviceRepo.Add(device)

	json.NewEncoder(w).Encode(id)
}

func Get(w http.ResponseWriter, r *http.Request) {
	var device models.Device
	params := mux.Vars(r)

	id, _ := strconv.Atoi(params["id"])
	arg := models.Device{ID: id}
	device, _ = deviceRepo.Get(arg)

	json.NewEncoder(w).Encode(device)
}

func Update(w http.ResponseWriter, r *http.Request) {
	var deviceJson models.DeviceJson
	params := mux.Vars(r)

	id, _ := strconv.Atoi(params["id"])
	body, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(body, &deviceJson)

	device := models.Device{
		ID:        id,
		Settings:  deviceJson.Settings,
		IsActive:  deviceJson.IsActive,
		UpdatedAt: deviceJson.UpdatedAt,
	}

	rowsUpdated := deviceRepo.Update(device)
	json.NewEncoder(w).Encode(rowsUpdated)
}

func Delete(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])

	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid Device ID")
		return
	}

	device := models.Device{ID: id}
	rowsDeleted := deviceRepo.Remove(device)
	json.NewEncoder(w).Encode(rowsDeleted)
}

func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
