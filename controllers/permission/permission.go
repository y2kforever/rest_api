package permission

import (
	"encoding/json"
	"github.com/abdugania/api/models"
	"github.com/abdugania/api/repository/permission"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"
	"strconv"
)

var permissionRepo permissionRepository.PermissionRepository

func All(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	count, _ := strconv.Atoi(params["count"])
	start, _ := strconv.Atoi(params["start"])

	if count > 10 || count < 1 {
		count = 10
	}
	if start < 0 {
		start = 0
	}

	arg := models.PermissionSearch{Start: start, Count: count}
	permissions, _ := permissionRepo.GetAll(arg)
	js, err := json.Marshal(permissions)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func Add(w http.ResponseWriter, r *http.Request) {
	var permissionJson models.PermissionJson
	body, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(body, &permissionJson)

	permission := models.Permission{
		Name:        permissionJson.Name,
		Alias:       permissionJson.Alias,
		Description: permissionJson.Description,
	}

	id, _ := permissionRepo.Add(permission)

	json.NewEncoder(w).Encode(id)
}

func Get(w http.ResponseWriter, r *http.Request) {
	var permission models.Permission
	params := mux.Vars(r)

	id, _ := strconv.Atoi(params["id"])
	arg := models.Permission{ID: id}
	permission, _ = permissionRepo.Get(arg)

	json.NewEncoder(w).Encode(permission)
}

func Update(w http.ResponseWriter, r *http.Request) {
	var permissionJson models.PermissionJson
	params := mux.Vars(r)

	id, _ := strconv.Atoi(params["id"])
	body, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(body, &permissionJson)

	permission := models.Permission{
		ID:          id,
		Name:        permissionJson.Name,
		Alias:       permissionJson.Alias,
		Description: permissionJson.Description,
		UpdatedAt:   permissionJson.UpdatedAt,
	}

	rowsUpdated := permissionRepo.Update(permission)
	json.NewEncoder(w).Encode(rowsUpdated)
}

func Delete(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])

	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid User ID")
		return
	}

	permission := models.Permission{ID: id}
	rowsDeleted := permissionRepo.Remove(permission)
	json.NewEncoder(w).Encode(rowsDeleted)
}

func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
