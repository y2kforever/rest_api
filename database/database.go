package database

import (
	"github.com/abdugania/api/conf"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"sync"
)

var (
	dbConn *gorm.DB
	once   sync.Once
)

func GetDb() *gorm.DB {

	cfg := conf.GetConf()
	once.Do(func() {
		var err error
		dbConn, err = gorm.Open(cfg.Databases.Dialect, cfg.Databases.GetDSN())
		if err != nil {
			panic(err)
		}
	})

	return dbConn
}
