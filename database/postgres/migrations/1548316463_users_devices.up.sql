CREATE TABLE IF NOT EXISTS "auth"."users_devices" (
  "user_id" int NOT NULL,
  "device_id" int NOT NULL,
  "last_active" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

SELECT auth.create_foreign_key('users_devices', 'user_id', 'users', 'id');

SELECT auth.create_foreign_key('users_devices', 'device_id', 'devices', 'id');