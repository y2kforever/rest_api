CREATE TABLE IF NOT EXISTS "auth"."logs" (
  "id" serial PRIMARY KEY,
  "name" varchar NOT NULL,
  "body" jsonb NOT NULL,
  "created_at" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);