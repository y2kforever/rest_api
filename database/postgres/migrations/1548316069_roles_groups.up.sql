CREATE TABLE IF NOT EXISTS "auth"."roles_groups" (
  "role_id" int NOT NULL,
  "group_id" int NOT NULL
);

SELECT auth.create_foreign_key('roles_groups', 'role_id', 'roles', 'id');

SELECT auth.create_foreign_key('roles_groups', 'group_id', 'groups', 'id');