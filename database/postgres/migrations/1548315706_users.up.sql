CREATE TABLE IF NOT EXISTS "auth"."users" (
  "id" serial PRIMARY KEY,
  "status_id" integer NOT NULL,
  "settings" jsonb,
  "created_at" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updated_at" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

select auth.create_foreign_key('users', 'status_id', 'user_statuses', 'id');