CREATE OR REPLACE FUNCTION auth.create_foreign_key(
  t_name         varchar,
  c_name         varchar,
  f_table_name   varchar,
  f_table_column varchar
)
  RETURNS VOID AS $$
BEGIN

  IF NOT EXISTS(SELECT 1
                FROM information_schema.table_constraints tc
                       INNER JOIN information_schema.constraint_column_usage ccu USING (constraint_catalog, constraint_schema, constraint_name)
                       INNER JOIN information_schema.key_column_usage kcu USING (constraint_catalog, constraint_schema, constraint_name)
                WHERE tc.table_name = "t_name"
                  AND constraint_type = 'FOREIGN KEY'
                  AND ccu.table_name = "f_table_name")
  THEN

    EXECUTE format(
        'ALTER TABLE IF EXISTS auth.%I ADD FOREIGN KEY (%I) REFERENCES auth.%I (%I)',
        t_name,
        c_name,
        f_table_name,
        f_table_column
    );

  END IF;

END;
$$
LANGUAGE plpgsql;