CREATE TABLE IF NOT EXISTS "auth"."users_roles" (
  "user_id" int NOT NULL,
  "role_id" int NOT NULL
);

SELECT auth.create_foreign_key('users_roles', 'user_id', 'users', 'id');

SELECT auth.create_foreign_key('users_roles', 'role_id', 'roles', 'id');