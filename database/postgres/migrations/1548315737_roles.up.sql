CREATE TABLE IF NOT EXISTS "auth"."roles" (
  "id" serial PRIMARY KEY,
  "name" jsonb NOT NULL,
  "alias" varchar NOT NULL,
  "description" jsonb NOT NULL,
  "is_active" boolean NOT NULL DEFAULT TRUE,
  "created_at" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updated_at" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);