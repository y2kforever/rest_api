CREATE TABLE IF NOT EXISTS "auth"."web_agents" (
  "id" serial PRIMARY KEY,
  "body" jsonb NOT NULL
);