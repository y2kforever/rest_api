CREATE TABLE IF NOT EXISTS "auth"."sessions" (
  "device_id" int NOT NULL,
  "agent_id" int NOT NULL,
  "ip" varchar NOT NULL
);

SELECT auth.create_foreign_key('sessions', 'device_id', 'devices', 'id');

SELECT auth.create_foreign_key('sessions', 'agent_id', 'web_agents', 'id');
