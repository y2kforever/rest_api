CREATE TABLE IF NOT EXISTS "auth"."users_logs" (
  "user_id" int NOT NULL,
  "log_id" int NOT NULL
);

SELECT auth.create_foreign_key('users_logs', 'user_id', 'users', 'id');

SELECT auth.create_foreign_key('users_logs', 'log_id', 'logs', 'id');