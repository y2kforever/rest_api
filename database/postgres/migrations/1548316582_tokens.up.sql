CREATE TABLE IF NOT EXISTS "auth"."tokens" (
  "id" serial PRIMARY KEY,
  "token" varchar NOT NULL,
  "refresh" varchar NOT NULL,
  "is_active" boolean NOT NULL DEFAULT TRUE,
  "expired_at" timestamp NOT NULL,
  "created_at" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE UNIQUE INDEX IF NOT EXISTS tokens_token_uindex ON auth.tokens (token);