CREATE TABLE IF NOT EXISTS "auth"."devices" (
  "id" serial PRIMARY KEY,
  "settings" jsonb NOT NULL,
  "is_active" boolean NOT NULL DEFAULT TRUE,
  "created_at" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updated_at" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);