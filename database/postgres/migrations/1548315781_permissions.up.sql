CREATE TABLE IF NOT EXISTS "auth"."permissions" (
  "id" serial PRIMARY KEY,
  "name" jsonb NOT NULL,
  "alias" varchar NOT NULL,
  "description" jsonb NOT NULL,
  "created_at" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updated_at" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);