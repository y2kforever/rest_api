CREATE TABLE IF NOT EXISTS "auth"."user_statuses" (
  "id" serial PRIMARY KEY,
  "name" jsonb NOT NULL,
  "alias" varchar NOT NULL,
  "description" jsonb NOT NULL
);

INSERT INTO "auth"."user_statuses" ("name", "alias", "description") VALUES
      ('{
      "ru": "Активный",
      "en": "Active"
      }'::jsonb,
      'active',
      '{
      "ru": "Акстивный статус",
      "en": "Active status"
      }'),       ('{
      "ru": "Не активный",
      "en": "Not active"
      }'::jsonb,
      'deactivate',
      '{
      "ru": "Не активный статус",
      "en": "Not active status"
      }');