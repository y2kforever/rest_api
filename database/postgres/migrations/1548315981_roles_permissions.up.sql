CREATE TABLE IF NOT EXISTS "auth"."roles_permissions" (
  "role_id" int NOT NULL,
  "permission_id" int NOT NULL
);

SELECT auth.create_foreign_key('roles_permissions', 'role_id', 'roles', 'id');

SELECT auth.create_foreign_key('roles_permissions', 'permission_id', 'permissions', 'id');