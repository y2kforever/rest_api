CREATE TABLE IF NOT EXISTS "auth"."devices_tokens" (
  "device_id" int NOT NULL,
  "token_id" int NOT NULL
);

SELECT auth.create_foreign_key('devices_tokens', 'device_id', 'devices', 'id');

SELECT auth.create_foreign_key('devices_tokens', 'token_id', 'tokens', 'id');